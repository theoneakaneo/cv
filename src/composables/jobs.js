import { computed, reactive, toRefs } from "vue";

const state = reactive({
  error: null,
  jobs: null,
  selectedJob: null,
  selectedIndex: null,
  loaded: false,
  loading: false,
});

export default function useJobs() {
  const load = () => {
    
    if (!state.loaded) {
      // try {
        // const jobsResponse = await fetch(
        //   "https://reqres.in/api/users?delay=2"
        // );
        const jobsResponse = 
            [
              { 
                id: 1, 
                cardType: "app", 
                logoUrl: "/img/logos/dr-gaming-technology-purple.jpg",
                imgUrl: "/img/blackjack11s-overhead.jpg",
                imgPositionX: "center",
                imgPositionY: "center center",
                title: "Jackpot 11's overhead screens",
                shortMsg: `Via diverse "overhead" schermen wordt reclame gemaakt, informatie gedeeld en worden gebeurtenissen aangekondigd voor dit casino kaartspel.`,
                msg: `
                Via diverse "overhead" schermen wordt reclame gemaakt, informatie gedeeld en worden gebeurtenissen aangekondigd voor dit casino kaartspel.
                <br><br>
                Afwisselend worden o.a. teaser-filmpjes, te winnen bedragen en voorbije winnaarsbedragen getoond. 
                <br><br>
                En bij winst zal een animatie starten om het gewonnen bedrag en de winnaar aan te duiden.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Werkt samen met de "dealer" console.
                </span>
                `,
                siteUrl: null,
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: `css`,
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "javascript",
                  },
                  {
                    id: 5,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                  {
                    id: 6,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: `video`,
                  },
                ],
              },
              { 
                id: 2, 
                cardType: "website", 
                logoUrl: "/img/logos/hr-technologies-white.png",
                imgUrl: "/img/www.werkeninazalma.be_front_nl_.png",
                imgPosition: "center top",
                title: "az alma jobsite",
                shortMsg: `Sollicitatie-website voor az alma gebouwd m.b.v. vue.js en tailwindcss.`,
                msg: `
                Een sollicitatie-website voor az alma.
                <br><br>
                Stylen op maat van homepage, job overzicht, job detail, sollicitatie formulier, faq en kandidaat-dashboard.
                <br><br>
                Binnen de begrenzingen van het "standaard"-product, zo goed mogelijk op maat van de klant afstemmen m.b.v. custom css.
                <br><br>
                Met o.a. "hero"-sectie en begeleidende foto's en teksten op maat.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Gebouwd in vue.js en tailwindcss.
                  Interactie via json met laravel API en classic asp endpoints.
                </span>
                `,
                siteUrl: "https://www.werkeninazalma.be/front/nl/",
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "tailwindcss",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "vue.js",
                  },
                  {
                    id: 5,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
              { 
                id: 3, 
                cardType: "website", 
                logoUrl: "/img/logos/hr-technologies-white.png",
                imgUrl: "/img/jobs.ethias.be_front_nl_.png",
                imgPosition: "center top",
                title: "ethias jobsite",
                shortMsg: `Sollicitatie-website voor ethias gebouwd m.b.v. vue.js en tailwindcss.`,
                msg: `
                Een sollicitatie-website voor ethias.
                <br><br>
                Stylen op maat van homepage, job overzicht, job detail, sollicitatie formulier, faq en kandidaat-dashboard.
                <br><br>
                Binnen de begrenzingen van het "standaard"-product, zo goed mogelijk op maat van de klant afstemmen m.b.v. custom css.
                <br><br>
                Met o.a. "hero"-sectie en begeleidende foto's en teksten op maat.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Gebouwd in vue.js en tailwindcss.
                  Interactie via json met laravel API en classic asp endpoints.
                </span>
                `,
                siteUrl: "https://jobs.ethias.be/front/nl",
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "tailwindcss",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "vue.js",
                  },
                  {
                    id: 5,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
              { 
                id: 4, 
                cardType: "app", 
                logoUrl: "/img/logos/dr-gaming-technology-purple.jpg",
                imgUrl: "/img/drgt-slots-interface.jpg",
                imgPosition: "center top",
                title: "Slot machines interface",
                shortMsg: `Interactie en delen van teaser-filmpjes, te winnen bedragen, voorbije winnaarsbedragen en -machines. Via geïntegreerde en zelfstandige "overhead"-schermen.`,
                msg: `
                Via extra aangesloten scherm(en) op slot machines en "overhead" schermen in de zaal wordt reclame gemaakt, informatie gedeeld, gebeurtenissen aangekondigd voor betreffende de aangesloten machines en kan men transacties/bedieningen uitvoeren.
                <br>
                Afwisselend worden o.a. teaser-filmpjes, te winnen bedragen, voorbije winnaarsbedragen en -machines getoond. 
                <br><br>
                En bij winst zal een animatie starten op alle aangesloten machines om het gewonnen bedrag en de winnaar aan te duiden.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Aangepaste animatie per machine voor winnaar versus niet-winnaars.
                </span>
                `,
                siteUrl: null,
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "css",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "javascript",
                  },
                  {
                    id: 4,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
              { 
                id: 5, 
                cardType: "app", 
                logoUrl: "/img/logos/dr-gaming-technology-purple.jpg",
                imgUrl: "/img/blackjack11s-console.jpg",
                imgPosition: "center top",
                title: "Jackpot 11's dealers console",
                shortMsg: `Aansturen van dit casino-kaartspel door de dealer via een touch-interface.`,
                msg: `
                Bedieningspaneel voor de dealer bij dit casino-kaartspel.
                <br><br>
                Paneel past zich aan naargelang configuratie (aantal spelers) en gebruikte resolutie.
                <br><br>
                Het verloop van het spel wordt via touch-interface gecontroleerd. (actieve spelers, inzet, winnaar, etc)
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Werkt samen met de "overhead" schermen.
                </span>
                `,
                siteUrl: null,
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "css",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "javascript",
                  },
                  {
                    id: 4,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
              { 
                id: 6, 
                cardType: "website", 
                logoUrl: "/img/logos/hr-technologies-white.png",
                imgUrl: "/img/compassgroup.hr-technologies.com_content_jobpage.asp_a=HOME&l=DUTCH(iPhone X).png",
                imgPosition: "center top",
                title: "Compass Group jobsite",
                shortMsg: `Sollicitatie-website voor Compass Group gebouwd m.b.v. classic asp, vue.js en tailwindcss.`,
                msg: `
                Sollicitatie-website op maat.
                <br><br>
                Binnen de mogelijkheden van de gebruikte technologie en binnen de begrenzingen opgelegd door een designer, op maat van de klant afstemmen.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Gebouwd in classic asp, vue.js, tailwindcss.
                  Data communicatie van/naar m.b.v. json, sql en classic asp.
                </span>
                `,
                siteUrl: "http://compassgroup.hr-technologies.com/content/jobpage.asp?a=HOME&l=DUTCH",
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "tailwindcss",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "vue.js",
                  },
                  {
                    id: 3,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "classic asp",
                  },
                  {
                    id: 5,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
              { 
                id: 7, 
                cardType: "website", 
                logoUrl: "/img/logos/hr-technologies-white.png",
                imgUrl: "/img/azsintblasius.hr-technologies.com_front_nl_.png",
                imgPosition: "center top",
                title: "az Sint-Blasius jobsite",
                shortMsg: `Sollicitatie-website voor az Sint-Blasius gebouwd m.b.v. vue.js en tailwindcss.`,
                msg: `
                Een sollicitatie-website voor az Sint-Blasius.
                <br><br>
                Stylen op maat van homepage, job overzicht, job detail, sollicitatie formulier, faq en kandidaat-dashboard.
                <br><br>
                Binnen de begrenzingen van het "standaard"-product, zo goed mogelijk op maat van de klant afstemmen m.b.v. custom css.
                <br><br>
                Met o.a. "hero"-sectie en begeleidende foto's en teksten op maat.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Gebouwd in vue.js en tailwindcss.
                  Interactie via json met laravel API en classic asp endpoints.
                </span>
                `,
                siteUrl: "https://azsintblasius.hr-technologies.com/front/nl/",
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "tailwindcss",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "vue.js",
                  },
                  {
                    id: 5,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
              { 
                id: 8, 
                cardType: "website", 
                logoUrl: "/img/logos/hr-technologies-white.png",
                imgUrl: "/img/jobs.idewe.be_front_nl_vacancies_1.png",
                imgPosition: "center top",
                title: "idewe jobsite",
                shortMsg: `Sollicitatie-website voor idewe gebouwd m.b.v. vue.js en tailwindcss.`,
                msg: `
                Een sollicitatie-website voor idewe.
                <br><br>
                Stylen op maat van homepage, job overzicht, job detail, sollicitatie formulier, faq en kandidaat-dashboard.
                <br><br>
                Binnen de begrenzingen van het "standaard"-product, zo goed mogelijk op maat van de klant afstemmen m.b.v. custom css.
                <br><br>
                Met o.a. "hero"-sectie en begeleidende foto's en teksten op maat.
                <br>
                <span class="text-sm leading-6 sm:text-base sm:leading-7 text-gray-400 flex justify-end mt-4">
                  Gebouwd in vue.js en tailwindcss.
                  Interactie via json met laravel API en classic asp endpoints.
                </span>
                `,
                siteUrl: "https://jobs.idewe.be/front/nl/about",
                tags: [
                  {
                    id: 1,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M7 17H2a2 2 0 0 1-2-2V2C0 .9.9 0 2 0h16a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-5l4 2v1H3v-1l4-2zM2 2v11h16V2H2z"/>
                    </svg>`,
                    txt: "tailwindcss",
                  },
                  {
                    id: 2,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M.7 9.3l4.8-4.8 1.4 1.42L2.84 10l4.07 4.07-1.41 1.42L0 10l.7-.7zm18.6 1.4l.7-.7-5.49-5.49-1.4 1.42L17.16 10l-4.07 4.07 1.41 1.42 4.78-4.78z"/>
                    </svg>`,
                    txt: "vue.js",
                  },
                  {
                    id: 5,
                    svg: `<svg class="flex-shrink-0 mr-1.5 h-4 w-4 text-gray-400" viewBox="0 0 20 20" fill="currentColor" stroke="none">
                      <path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                    </svg>`,
                    txt: "json",
                  },
                ],
              },
            ]
        ;
        state.jobs = jobsResponse;//await jobsResponse.json();
        
      // } catch (e) {
      //   state.error = e;
      // }
    }

    // console.log(state.jobs);
    
  };

  const loadById = (id) => {
    state.selectedJob = state.jobs.find(job => job.id == id);
    state.selectedIndex = state.jobs.findIndex(job => job.id == id);
    return state.selectedJob;
  };


  const next = () => {
    if (state.selectedIndex < state.jobs.length - 1) { 
      state.selectedIndex = state.selectedIndex + 1;
      state.selectedJob = state.jobs[state.selectedIndex];
    }
  };
  const prev = () => {
    if (state.selectedIndex > 0) { 
      state.selectedIndex = state.selectedIndex - 1;
      state.selectedJob = state.jobs[state.selectedIndex];
    }
  };
  const last = () => {
    state.selectedIndex = state.jobs.length - 1;
    state.selectedJob = state.jobs[state.selectedIndex];
  };
  const first = () => {
    state.selectedIndex = 0;
    state.selectedJob = state.jobs[state.selectedIndex];
  };

  // const hasNext = () => {
  //   return ;
  // }

  // const hasPrev = () => {
  //   return computed(() => state.selectedIndex > 0);
  // }

  return { 
    ...toRefs(state), 
    load,
    loadById,
    next,
    prev, 
    last,
    first,
    hasNext: computed(() => state.selectedIndex < state.jobs.length - 1),
    hasPrev: computed(() => state.selectedIndex > 0),
  };
}