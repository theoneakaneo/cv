import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './index.css'

// import AOS from 'aos';
// import 'aos/dist/aos.css';

// createApp(App)
//   .use(router)
//   .mount('#app')

const app = createApp(App)
app.use(router)

router.isReady().then(() => app.mount('#app'))

// AOS.init({
//   // once: true,
//   // easing: 'ease-in-out',
//   // mirror: true,
// });