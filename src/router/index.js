import { createWebHistory, createRouter } from 'vue-router';
import Cv from '../views/Cv.vue';
import Skills from '../views/Skills.vue';
import Tech from '../views/Tech.vue';
import Xp from '../views/Xp.vue';
import Edu from '../views/Edu.vue';

import Projects from '../views/Projects.vue';

import Project from '../components/Project.vue';

import NotFound from '../views/NotFound.vue';

const routes = [
  {
    path: '/',
    name: 'Cv',
    component: Cv,
  },
  {
    path: '/skills',
    name: 'Skills',
    component: Skills,
  },
  {
    path: '/tech',
    name: 'Tech',
    component: Tech,
  },
  {
    path: '/edu',
    name: 'Edu',
    component: Edu,
  },
  {
    path: '/xp',
    name: 'Xp',
    component: Xp,
  },
  {
    path: '/projects',
    name: 'Projects',
    component: Projects,
  },
  {
    path: '/projects/:id',
    name: 'Project',
    component: Project,
    props: true,
  },
  {
    path: "/:catchAll(.*)",
    component: NotFound,
  },
];


const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition;
    } else if (to.hash) {
      return {
        selector: to.hash
      };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

// router.afterEach((to, from) => {
//   window.scroll({top:0, left:0, behavior:'smooth'})
// })

// router.beforeEach((to, from, next) => { 
//   setTimeout(() => {
//       console.log('test')
//       window.scroll({top:0, left:0, behavior:'smooth'})
//   }, 100);
//   next();
// });

export default router;