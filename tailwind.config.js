const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
  ],
  theme: {
    lineClamp: {
      1: 1,
      2: 2,
      3: 3
    },
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      animation: {
        'fade-once': 'fade 3s ease',
      },
      keyframes: {
        fade: {
          '0%': {  opacity: '0',  },
          '100%': { opacity: '1' },
        }
      }
    },
  },
  variants: {
    lineClamp: ["responsive"]
  },
  plugins: [
    require('@tailwindcss/ui')({
      layout: 'sidebar',
    }),
    require('tailwindcss-line-clamp')
  ],
}
